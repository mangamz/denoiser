#include <iostream>
#include "Denoiser.h"

//#include "functions.h"
OIDN_NAMESPACE_USING

void errorCallback(void* userPtr, oidn::Error error, const char* message)
{
    throw std::runtime_error(message);
}

bool progressCallback(void* userPtr, double n)
{
    std::cout << (int)(n * 100.0) << "% complete\n";
    return true;
}

Denoiser::Denoiser()
{
    _affinity = true;
    _hdr = true;
    _srgb = false;
    _num_runs = 1;
    _num_threads = 0;
    _maxmem = -1;
    _clean_aux = false;

    _prefilter = false;
}

void Denoiser::init(
    bool affinity,
    bool hdr,
    bool srgb,
    unsigned int num_runs,
    int num_threads,
    int maxmem,
    bool clean_aux,
    bool prefilter
)
{
    _affinity = affinity;
    _hdr = hdr;
    _srgb = srgb;
    _num_runs = num_runs;
    _num_threads = num_threads;
    _maxmem = maxmem;
    _clean_aux = clean_aux;
    _prefilter = prefilter;

    //if prefilter is true then aux images (albedo and normal) will be noise-free
    //and so clean_aux should be true
    if (_prefilter)
        _clean_aux = true;

    //_device = oidn::newDevice(OIDN_DEVICE_TYPE_CPU);
    _device = oidn::newDevice(OIDN_DEVICE_TYPE_DEFAULT);
    //_device = oidn::newDevice();

    const char* errorMessage;
    if (_device.getError(errorMessage) != oidn::Error::None)
        throw std::runtime_error(errorMessage);
    _device.setErrorFunction(errorCallback);
    // Set our _device parameters
    if (_num_threads)
        _device.set("numThreads", _num_threads);
    if (_affinity)
        _device.set("setAffinity", _affinity);

    // Commit the changes to the device
    _device.commit();

}

oidn::Format Denoiser::getFormat(int channels)
{
    //only the Float3 format is supporterd
    return oidn::Format::Float3;
    /*
    switch (channels)
    {
    case 0:
        return oidn::Format::Undefined;
    case 1:
        return oidn::Format::Float;
    case 2:
        return oidn::Format::Float2;
    case 3:
        return oidn::Format::Float3;
    case 4:
        return oidn::Format::Float4;
    default:
        return oidn::Format::Undefined;
    }
    */
}

bool Denoiser::denoise(MultiChannelEXR& exr)
{
    try
    {
        std::cout << "\nInitializing OIDN\n";
        // Create our device
        //oidn::DeviceRef device = oidn::newDevice(OIDN_DEVICE_TYPE_CPU);
        //oidn::DeviceRef device = oidn::newDevice(OIDN_DEVICE_TYPE_DEFAULT);
        /*
        const char* errorMessage;
        if (_device.getError(errorMessage) != oidn::Error::None)
            throw std::runtime_error(errorMessage);
        _device.setErrorFunction(errorCallback);
        // Set our _device parameters
        if (_num_threads)
            _device.set("numThreads", _num_threads);
        if (_affinity)
            _device.set("setAffinity", _affinity);

        // Commit the changes to the device
        _device.commit();
        */
        DeviceType deviceType = _device.get<DeviceType>("type");
        const int versionMajor = _device.get<int>("versionMajor");
        const int versionMinor = _device.get<int>("versionMinor");
        const int versionPatch = _device.get<int>("versionPatch");

        //PrintInfo("Using OIDN version %d.%d.%d", versionMajor, versionMinor, versionPatch);
        //std::cout << "Using OIDN version " << versionMajor << "." << versionMinor << "." << versionPatch << std::endl;
        std::cout << "device = " << (int)deviceType << ", prefilter=" << _prefilter << std::endl;
        std::cout << "version=" << versionMajor << "." << versionMinor << "." << versionPatch << std::endl;


        bool hasAlbedo = exr.hasLayer(MultiChannelEXR::albedoName);
        bool hasNormal = exr.hasLayer(MultiChannelEXR::normalName);
        //void* albedoPtr = exr.getPixels(MultiChannelEXR::albedoName);
        //void* normalPtr = exr.getPixels(MultiChannelEXR::normalName);

        // If a normal AOV is loaded then we also require an albedo AOV
        if (hasNormal && !hasAlbedo)
            hasNormal = false;

        if (_prefilter)
        {
            // Create a separate filter for denoising an auxiliary albedo image (in-place)
            if (hasAlbedo)
            {
                std::cout << "\ndenoising aux layer " << MultiChannelEXR::albedoName << std::endl;
                oidn::Format format = getFormat(exr.getChannelCount(MultiChannelEXR::albedoName));
                oidn::FilterRef albedoFilter = _device.newFilter("RT");
                albedoFilter.setProgressMonitorFunction((oidn::ProgressMonitorFunction)progressCallback);
                //albedoFilter.setImage("albedo", albedoPtr, format, exr._width, exr._height);
                //albedoFilter.setImage("output", albedoPtr, format, exr._width, exr._height);
                albedoFilter.setImage("albedo", exr.getBuffer(MultiChannelEXR::albedoName), format, exr._width, exr._height);
                albedoFilter.setImage("output", exr.getBuffer(MultiChannelEXR::albedoName), format, exr._width, exr._height);
                albedoFilter.commit();
                
                // Prefilter the auxiliary images
                albedoFilter.execute();
            }

            // Create a separate filter for denoising an auxiliary normal image (in-place)
            if (hasNormal)
            {
                std::cout << "\ndenoising aux layer " << MultiChannelEXR::normalName << std::endl;
                oidn::Format format = getFormat(exr.getChannelCount(MultiChannelEXR::normalName));
                oidn::FilterRef normalFilter = _device.newFilter("RT");
                normalFilter.setProgressMonitorFunction((oidn::ProgressMonitorFunction)progressCallback);
                //normalFilter.setImage("normal", normalPtr, format, exr._width, exr._height);
                //normalFilter.setImage("output", normalPtr, format, exr._width, exr._height);
                normalFilter.setImage("normal", exr.getBuffer(MultiChannelEXR::normalName), format, exr._width, exr._height);
                normalFilter.setImage("output", exr.getBuffer(MultiChannelEXR::normalName), format, exr._width, exr._height);
                normalFilter.commit();
                
                // Prefilter the auxiliary images
                normalFilter.execute();
            }
        }


        std::map<std::string, Layer>& layers = exr.layers();

        for (std::map<std::string, Layer>::iterator it = layers.begin(); it != layers.end(); it++)
        {
            Layer& layer = it->second;
            if(layer.name != MultiChannelEXR::albedoName && layer.name != MultiChannelEXR::normalName)
                denoise(exr, layer.name);
        }
    }
    catch (const std::exception& e)
    {
        std::cout << "[OIDN]: " << e.what() << std::endl;
        return false;
    }

    return true;
}

bool Denoiser::denoise(MultiChannelEXR& exr, std::string& channelname)
{
    // Create the AI filter
    oidn::FilterRef filter = _device.newFilter("RT");

    // Set our progress callback
    filter.setProgressMonitorFunction((oidn::ProgressMonitorFunction)progressCallback);

     // Set our the filter images
    std::cout << "\ndenoising layer " << channelname << std::endl;

    bool hasAlbedo = exr.hasLayer(MultiChannelEXR::albedoName);
    bool hasNormal = exr.hasLayer(MultiChannelEXR::normalName);

    // If a normal AOV is loaded then we also require an albedo AOV
    if (hasNormal && !hasAlbedo)
        hasNormal = false;

    if(channelname == MultiChannelEXR::alphaName)
    {
        hasNormal = false;
        hasAlbedo = false;
        //return false;
    }

    oidn::Format format = getFormat(exr.getChannelCount(channelname));

    filter.setImage("color", exr.getBuffer(channelname), format, exr._width, exr._height);
    
    if (hasAlbedo)
    {
        oidn::Format format = getFormat(exr.getChannelCount(MultiChannelEXR::albedoName));
        if(_prefilter)
            filter.setImage("albedo", exr.getOutBuffer(MultiChannelEXR::albedoName), format, exr._width, exr._height);
        else
            filter.setImage("albedo", exr.getBuffer(MultiChannelEXR::albedoName), format, exr._width, exr._height);
    }
    if (hasNormal)
    {
        oidn::Format format = getFormat(exr.getChannelCount(MultiChannelEXR::normalName));
        if(_prefilter)
            filter.setImage("normal", exr.getOutBuffer(MultiChannelEXR::normalName), format, exr._width, exr._height);
        else
            filter.setImage("normal", exr.getBuffer(MultiChannelEXR::normalName), format, exr._width, exr._height);
    }
    
    //TODO creare un buffer di output
    filter.setImage("output", exr.getOutBuffer(channelname), format, exr._width, exr._height);

    filter.set("hdr", _hdr);
    filter.set("srgb", _srgb);
    if (_maxmem >= 0)
        filter.set("maxMemoryMB", _maxmem);
    filter.set("cleanAux", _clean_aux);

    // Commit changes to the filter
    filter.commit();

    // Execute denoise
    int sum = 0;
    for (unsigned int i = 0; i < _num_runs; i++)
    {
        //PrintInfo("Denoising...");
        clock_t start = clock(), diff;
        filter.execute();
        diff = clock() - start;
        int msec = diff * 1000 / CLOCKS_PER_SEC;

        //if (_num_runs > 1)
            //PrintInfo("Denoising run %d complete in %d.%03d seconds", i + 1, msec / 1000, msec % 1000);
        //else
            //PrintInfo("Denoising complete in %d.%03d seconds", msec / 1000, msec % 1000);

        sum += msec;
    }
    if (_num_runs > 1)
    {
        sum /= _num_runs;
        //PrintInfo("Denoising avg of %d complete in %d.%03d seconds", _num_runs, sum / 1000, sum % 1000);
    }

    return true;
}

