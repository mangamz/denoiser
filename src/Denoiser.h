#pragma once

#include <string>
#include "MultiChannelEXR.h"
#include <OpenImageDenoise/oidn.hpp>

class Denoiser
{
public:
    Denoiser();
    void init(
        bool affinity,
        bool hdr,
        bool srgb,
        unsigned int num_runs,
        int num_threads,
        int maxmem,
        bool clean_aux,
        bool prefilter
    );

    oidn::DeviceRef& getDevice() {return _device;}
    bool denoise(MultiChannelEXR& exr);

private:
    bool denoise(MultiChannelEXR& exr, std::string& channelname);
    oidn::Format getFormat(int channels);
private:
    bool _affinity;
    bool _hdr;
    bool _srgb;
    unsigned int _num_runs;
    int _num_threads;
    int _maxmem;
    bool _clean_aux;//true if auxsiliary images (albedo and normal) are already noise free

    bool _prefilter;//not used anymore, to remove

    oidn::DeviceRef _device;
};

