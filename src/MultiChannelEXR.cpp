#include "MultiChannelEXR.h"
#include <iostream>

std::string MultiChannelEXR::beautyName = "RGB";
std::string MultiChannelEXR::alphaName = "Alpha";
std::string MultiChannelEXR::normalName = "normal";
std::string MultiChannelEXR::albedoName = "albedo";


Layer::Layer(/*const oidn::DeviceRef& device, */const std::string& Name, int Size, int Offset, int w, int h)
    :name(Name),size(Size),offset(Offset),width(w), height(h)
{
}

Layer::~Layer()
{
    if (hostPtr != devPtr)
      //free(hostPtr);
      delete [] hostPtr;
}

void Layer::createBuffers(const oidn::DeviceRef& device)
{
    oidn::Storage storage = oidn::Storage::Undefined;
    //Format dataType = Format::Float

    //All the buffers must be created with numChannels = 3 because
    //only the Float3 format is supported;
    //For the alpha buffer the single alpha channel will be duplicated 3 times
    //to obtain the Float3 format
    //int numChannels = size;
    int numChannels = 3;

    size_t valueByteSize = sizeof(float);
    size_t numValues = size_t(width) * height * numChannels;
    byteSize = std::max(numValues * valueByteSize, size_t(1)); // avoid zero-sized buffer

    //std::cout << "numValues:  " << numValues << std::endl;
    //std::cout << "byteSize:  " << byteSize << std::endl;

    byteSize = std::max(numValues * valueByteSize, size_t(1)); // avoid zero-sized buffer
    
    //buffer = device.newBuffer(byteSize, storage);
    buffer = device.newBuffer(byteSize);
    storage = buffer.getStorage(); // get actual storage mode
    devPtr = static_cast<float*>(buffer.getData());
    //hostPtr = (storage != oidn::Storage::Device) ? devPtr : static_cast<float*>(malloc(byteSize));
    hostPtr = (storage != oidn::Storage::Device) ? devPtr : new float[numValues];

    out_buffer = device.newBuffer(byteSize);
    storage = buffer.getStorage(); // get actual storage mode
    out_devPtr = static_cast<float*>(out_buffer.getData());
    //out_hostPtr = (storage != oidn::Storage::Device) ? out_devPtr : static_cast<float*>(malloc(byteSize));
    out_hostPtr = (storage != oidn::Storage::Device) ? out_devPtr : new float[numValues];

}


MultiChannelEXR::MultiChannelEXR():_input_image(nullptr)
{

}

MultiChannelEXR::~MultiChannelEXR()
{
    cleanup();
}

void MultiChannelEXR::cleanup()
{
    delete _input_image;
    _input_image = nullptr;

}

void  MultiChannelEXR::printChannels()
{
    std::cout << "\nChannels\n";
    const OIIO::ImageSpec& spec = _input_image->spec();
    for (int i = 0; i < spec.nchannels; ++i)
        std::cout << "Channel " << i << " is " << spec.channelnames[i] << "\n";

}

bool MultiChannelEXR::findChannels(const oidn::DeviceRef& device)
{
    std::cout << "\nLayers\n";
    const OIIO::ImageSpec& spec = _input_image->spec();

    std::string szR = spec.channelnames[0];
    std::string szG = spec.channelnames[1];
    std::string szB = spec.channelnames[2];
    std::string szA = spec.channelnames[3];

    //The first 4 channels should always be RGBA
    if (spec.nchannels < 4 ||
        szR[0] != 'R' ||
        szG[0] != 'G' ||
        szB[0] != 'B' ||
        szA[0] != 'A'
        )
    {
        cleanup();
        std::cout << "Missing RGBA channels\n";
        return false;
    }

    //Layer layer(beautyName, 4, 0);
    Layer layer(beautyName, 3, 0, _width, _height);
    _layers[beautyName] = layer;

    Layer layerA(alphaName, 1, 3, _width, _height);
    _layers[alphaName] = layerA;

    std::string last_name(beautyName);
    int count = 0;
    for (int i = 4; i < spec.nchannels; ++i)
    {
        std::string name = spec.channelnames[i];
        int pos = (int)name.find_last_of(".");
        name = name.substr(0, pos);
        if (name != last_name)
        {
            Layer channel(name, 1, i, _width, _height);
            _layers[name] = channel;
            last_name = name;
        }
        else
        {
            _layers[name].size++;//???????
        }
    }

    for (auto it = _layers.begin(); it != _layers.end(); it++)
    {
        Layer& layer = it->second;
        std::cout << layer.name << " " << layer.size << " " << layer.offset << "\n";
        layer.createBuffers(device);
    }

    return true;
}

bool MultiChannelEXR::hasLayer(const std::string& layerName)
{
    return (_layers.count(layerName) > 0);
}

const oidn::BufferRef& MultiChannelEXR::getBuffer(const std::string& layerName)
{
    return _layers[layerName].buffer;
}

const oidn::BufferRef& MultiChannelEXR::getOutBuffer(const std::string& layerName)
{
    return _layers[layerName].out_buffer;
}

/*
void* MultiChannelEXR::getPixels(const std::string& layerName)
{
    if(_layers.count(layerName) > 0)
        //return _layers[layerName].pixels.data();
        return _layers[layerName].hostPtr;
    return nullptr;
}

void* MultiChannelEXR::output_pixels(const std::string& layerName)
{
    //if (_layers.count(layerName) > 0)
        //return _layers[layerName].out_pixels.data();
    return nullptr;
}
*/

bool MultiChannelEXR::createBuffers(const oidn::DeviceRef& device)
{
    //auto inp = OIIO::ImageInput::open(_in_path.c_str());
    _input_image = new OIIO::ImageBuf(_in_path.c_str());

    if (_input_image->init_spec(_in_path, 0, 0))
    {
        _roi = OIIO::get_roi_full(_input_image->spec());
        _width  = _roi.width();
        _height = _roi.height();

        printChannels();
        findChannels(device);
        
        int nchannels = _roi.nchannels();

        unsigned int size2 = _width * _height * nchannels;

        _input_pixels.resize(size2);
        
        _input_image->get_pixels(_roi, OIIO::TypeDesc::FLOAT, &_input_pixels[0]);
        
        //float* in = (float*)_input_pixels.data();
        //float* out = nullptr;
        float* in = (float*)_input_pixels.data();
        float* out = nullptr;

        std::cout << "\nresolution:    " << _width << " x " << _height <<"\n";
        std::cout << "nchannels: " << nchannels << "\n";

        int pixel = 0;
        for (unsigned int i = 0; i < _input_pixels.size(); i += nchannels)
        {
            for (auto it = _layers.begin(); it != _layers.end(); it++)
            {
                Layer& layer = it->second;
                //out = layer.pixels.data() + 3*pixel;
                //out = layer.pixels.data() + layer.size * pixel;
                //out = layer.hostPtr + layer.size * pixel;
                out = layer.hostPtr + 3 * pixel;
                
                memset(out, 0, 3);
                if (!convertToFormat(in + layer.offset, out, layer.size, 3))
                //if (!convertToFormat(in + layer.offset, out, layer.size, layer.size))
                {
                    std::cout << "Failed to convert\n";
                    cleanup();
                    return false;
                }
            }
            in += nchannels;
            pixel++;

        }
    }
    else
    {
        cleanup();
        return false;
    }
    return true;
}

bool MultiChannelEXR::update()
{
    int nchannels = _roi.nchannels();
    unsigned int size2 = _width * _height * nchannels;

    //std::cout << "update\n";
    //std::vector<float>& output_pixels = _layers["RGBA"].out_pixels;

    //float* in = nullptr;
    //float* out = (float*)_input_pixels.data();//original image
    float* in = nullptr;
    float* out = (float*)_input_pixels.data();//original image

    int size = _width * _height;
    for (unsigned int i = 0; i < size; ++i)
    {
        //for (unsigned int j = 0; j < _channels.size(); ++j)
        for (auto it = _layers.begin(); it != _layers.end(); it++)
        {
            Layer& layer = it->second;
            //in = layer.out_pixels.data() + 3 * i;
            //in = layer.out_pixels.data() + layer.size * i;
            //in = layer.hostPtr + layer.size * i;
            in = layer.out_hostPtr + 3 * i;
            
            
            if (layer.name == normalName || layer.name == albedoName)
            {
                //in = layer.pixels.data() + 3 * i;
                in = layer.hostPtr + 3 * i;
            }
            
            if (!convertToFormat(in, out + layer.offset, 3, layer.size))
            //if (!convertToFormat(in, out + layer.offset, layer.size, layer.size))
            {
                std::cout << "Failed to convert\n";
                cleanup();
                return false;
            }
        }
        out += nchannels;
    }

    // Set our OIIO pixels
    if (!_input_image->set_pixels(_roi, OIIO::TypeDesc::FLOAT, &_input_pixels[0]))
        return false;


    return save();
}
/*
bool MultiChannelEXR::update(const std::string& layerName)
{
    Layer& layer = _layers[layerName];

    int nchannels = _roi.nchannels();
    unsigned int size2 = _width * _height * nchannels;

    std::cout << "update " << layerName << std::endl;
    std::vector<float>& output_pixels = layer.out_pixels;

    float* in = (float*)output_pixels.data();
    float* out = (float*)_input_pixels.data();//original image

    unsigned int size = _width * _height;

    for (unsigned int i = 0; i < size; ++i)
    {
        if (!convertToFormat(in , out + layer.offset, 3, layer.size))
        {
            std::cout << "Failed to convert\n";
            cleanup();
            return false;
        }

        out += nchannels;
        in += 3;
    }

    // Set our OIIO pixels
    if (!_input_image->set_pixels(_roi, OIIO::TypeDesc::FLOAT, &_input_pixels[0]))
       return false;

    return save();
}
*/
bool MultiChannelEXR::save()
{
    // Save the output image
    //std::cout << "Saving to: " << _out_path << "\n";
    if (_input_image->write(_out_path))
        return true;
    else
        return false;

}
bool MultiChannelEXR::convertToFormat(void* in_ptr, void* out_ptr, unsigned int in_channels, unsigned int out_channels)
{
    //std::cout << "convertToFormat\n";
    switch (in_channels)
    {
    case(1):
    {
        switch (out_channels)
        {
        case(1):
        case(2):
        case(3):
        case(4): memcpy(out_ptr, in_ptr, sizeof(float)); return true;
        default: return false; // How has this happened?
        }
    }
    case(2):
    {
        switch (out_channels)
        {
        case(1): memcpy(out_ptr, in_ptr, sizeof(float)); return true;
        case(2):
        case(3):
        case(4): memcpy(out_ptr, in_ptr, 2 * sizeof(float)); return true;
        default: return false; // How has this happened?
        }
    }
    case(3):
    {
        switch (out_channels)
        {
        case(1): memcpy(out_ptr, in_ptr, 1 * sizeof(float)); return true;
        case(2): memcpy(out_ptr, in_ptr, 2 * sizeof(float)); return true;
        case(3):
        case(4): memcpy(out_ptr, in_ptr, 3 * sizeof(float)); return true;
        default: return false; // How has this happened?
        }
    }
    case(4):
    {
        switch (out_channels)
        {
        case(1): memcpy(out_ptr, in_ptr, 1 * sizeof(float)); return true;
        case(2): memcpy(out_ptr, in_ptr, 2 * sizeof(float)); return true;
        case(3): memcpy(out_ptr, in_ptr, 3 * sizeof(float)); return true;
        case(4): memcpy(out_ptr, in_ptr, 4 * sizeof(float)); return true;
        default: return false; // How has this happened?
        }
    }
    default: return false; // How has this happened?
    }
    return false; // some unsupported conversion
}
