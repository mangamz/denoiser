#pragma once

#include <OpenImageIO/imageio.h>
#include <OpenImageIO/imagebuf.h>
#include <OpenImageDenoise/oidn.hpp>
#include <vector>
#include <map>

class Layer
{
public:
	Layer() {}
	//TODO: mettere la costruzione dei buffers nel costruttore
	Layer(/*const oidn::DeviceRef& device, */const std::string& Name, int Size, int Offset, int w, int h);
	~Layer();
	void createBuffers(const oidn::DeviceRef& device);

	std::string name; //Channel name
	int size; //Number of elements of that channel (like rgba, rgb, xyz etc.)
	int offset; //offset of the first element of the channel
	int width;
	int height;
	size_t byteSize;

	oidn::BufferRef buffer;
	oidn::BufferRef out_buffer;

	float* devPtr = nullptr;
	float* hostPtr = nullptr;

	float* out_devPtr = nullptr;
	float* out_hostPtr = nullptr;

};

class MultiChannelEXR
{
public:
	MultiChannelEXR();
	~MultiChannelEXR();

	void setInput(const std::string& in_path) 
	{
		_in_path = in_path;
	}
	void setOutput(const std::string& out_path)
	{
		_out_path = out_path;
	}

	int getChannelCount(const std::string& layerName)
	{
		if (_layers.count(layerName) > 0)
			return _layers[layerName].size;
		return 0;
	}
	

	bool createBuffers(const oidn::DeviceRef& device);
	void printChannels();
	void cleanup();
	bool update();
	bool save();

	bool hasLayer(const std::string& layerName);
	const oidn::BufferRef& getBuffer(const std::string& layerName);
	const oidn::BufferRef& getOutBuffer(const std::string& layerName);


	//void* getPixels(const std::string& layerName);
	//void* output_pixels(const std::string& layerName);// { return (void*)_output_pixels.data(); }

	std::map<std::string, Layer>& layers() { return _layers; }

	int _width;
	int _height;

	static std::string beautyName;
	static std::string alphaName;
	static std::string normalName;
	static std::string albedoName;

private:
	bool findChannels(const oidn::DeviceRef& device);
	
	bool convertToFormat(void* in_ptr, void* out_ptr, unsigned int in_channels, unsigned int out_channels);
private:
	OIIO::ImageBuf* _input_image;
	std::string _in_path;
	std::string _out_path;
	OIIO::ROI _roi;

	std::map<std::string, Layer> _layers;

	std::vector<float> _input_pixels;

};

