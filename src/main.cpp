
#include <OpenImageDenoise/oidn.hpp>
#include <OpenImageIO/imageio.h>
#include <time.h>
#ifdef _WIN32
#include <thread>
#include <chrono>
#include <windows.h>
#include <winternl.h>
#endif

#include "MultiChannelEXR.h"
#include "Denoiser.h"


// Logging verbosity level
int verbosity = 2;

// Application start time
std::chrono::high_resolution_clock::time_point app_start_time;

std::string getTime()
{
    std::chrono::duration<double, std::milli> time_span = std::chrono::high_resolution_clock::now() - app_start_time;
    double milliseconds = time_span.count();
    int seconds = floor(milliseconds / 1000.0);
    int minutes = floor((float(seconds) / 60.f));
    milliseconds -= seconds * 1000.0;
    seconds -= minutes * 60;
    char s[10];
    sprintf(s, "%02d:%02d:%03d", minutes, seconds, (int)milliseconds);
    return std::string(s);
}


template<typename... Args>
void PrintInfo(const char* c, Args... args)
{
    if (!verbosity)
        return;
    char buffer[256];
    sprintf(buffer, c, args...);
    std::cout << getTime() << "       | " << buffer << std::endl;
}

template<typename... Args>
void PrintError(const char* c, Args... args)
{
    char buffer[256];
    sprintf(buffer, c, args...);
    std::cerr << getTime() << " ERROR | " << buffer << std::endl;
}


#ifdef _WIN32
int getSysOpType()
{
    int ret = 0;
    NTSTATUS(WINAPI * RtlGetVersion)(LPOSVERSIONINFOEXW);
    OSVERSIONINFOEXW osInfo;

    *(FARPROC*)&RtlGetVersion = GetProcAddress(GetModuleHandleA("ntdll"), "RtlGetVersion");

    if (NULL != RtlGetVersion)
    {
        osInfo.dwOSVersionInfoSize = sizeof(osInfo);
        RtlGetVersion(&osInfo);
        ret = osInfo.dwMajorVersion;
    }
    return ret;
}
#endif

void exitfunc(int exit_code)
{
#ifdef _WIN32
    if (getSysOpType() < 10)
    {
        HANDLE tmpHandle = OpenProcess(PROCESS_ALL_ACCESS, TRUE, GetCurrentProcessId());
        if (tmpHandle != NULL)
        {
            PrintInfo("terminating...");
            std::this_thread::sleep_for(std::chrono::seconds(1)); // delay 1s
            TerminateProcess(tmpHandle, 0);
        }
    }
#endif
    exit(exit_code);
}

void cleanup()
{
    //if (input_beauty) delete input_beauty;
    //if (input_albedo) delete input_albedo;
    //if (input_normal) delete input_normal;
}

void printParams()
{
    // Always print parameters if needed
    int old_verbosity = verbosity;
    verbosity = 1;
    PrintInfo("Command line parameters");
    PrintInfo("-v [int]        : log verbosity level 0:disabled 1:simple 2:full (default 2)");
    PrintInfo("-i [string]     : path to input image");
    PrintInfo("-o [string]     : path to output image");
    //PrintInfo("-a [string]     : path to input albedo AOV (optional)");
    //PrintInfo("-n [string]     : path to input normal AOV (optional, requires albedo AOV)");
    PrintInfo("-hdr [int]      : Image is a HDR image. Disabling with will assume the image is in sRGB (default 1 i.e. enabled)");
    PrintInfo("-srgb [int]     : whether the main input image is encoded with the sRGB (or 2.2 gamma) curve (LDR only) or is linear (default 0 i.e. disabled)");
    PrintInfo("-t [int]        : number of threads to use (defualt is all)");
    PrintInfo("-affinity [int] : Enable affinity. This pins virtual threads to physical cores and can improve performance (default 0 i.e. disabled)");
    PrintInfo("-repeat [int]   : Execute the denoiser N times. Useful for profiling.");
    PrintInfo("-maxmem [int]   : Maximum memory size used by the denoiser in MB");
    PrintInfo("-clean_aux [int]: Whether the auxiliary feature (albedo, normal) images are noise-free; recommended for highest quality but should *not* be enabled for noisy auxiliary images to avoid residual noise (default 0 i.e. disabled)");
    PrintInfo("-p [int]        : Whether the prefiltering of the aux AOVs (albedo and normal) is enabled");
    verbosity = old_verbosity;
}


int main(int argc, char* argv[])
{

    if (argc > 1)
    {
        for (int i = 1; i < argc; i++)
        {
            if (strcmp(argv[i], "-v"))
                continue;
            i++;
            if (i >= argc)
            {
                PrintError("incorrect number of arguments for flag -v");
            }
            verbosity = std::stoi(std::string(argv[i]));
            break;
        }
    }
    app_start_time = std::chrono::high_resolution_clock::now();

    // Pass our command line args
    //std::string out_path("C:/dev/Milk/IntelOIDenoiser/images/010_cra_1250_lighting_test_VEH_lightingDenoised_v045.1038.exr");
    //std::string in_path("C:/dev/Milk/IntelOIDenoiser/images/010_cra_1250_lighting_test_VEH_lighting_v045.1038.exr");
    std::string out_path;
    std::string in_path;

    bool affinity = false;
    bool hdr = true;
    bool srgb = false;
    unsigned int num_runs = 1;
    int num_threads = 0;
    int maxmem = -1;
    bool clean_aux = false;
    bool prefilter = false;

    if (argc == 1)
    {
        /*
        affinity  = true;
        hdr       = true;
        srgb      = false;
        num_threads = 0;
        num_runs = 1;
        maxmem = -1;
        clean_aux = false;
        prefilter = true;
        */
        printParams();
        exitfunc(EXIT_SUCCESS);
    }
    
    for (int i = 1; i < argc; i++)
    {
        const std::string arg(argv[i]);
        
        if (arg == "-i")
        {
            i++;
            in_path = std::string(argv[i]);
            if (verbosity >= 2)
                PrintInfo("Input image: %s", in_path.c_str());
        }
        else if (arg == "-o")
        {
            i++;
            out_path = std::string(argv[i]);
            if (verbosity >= 2)
                PrintInfo("Output image: %s\n", out_path.c_str());
        }
        else if (arg == "-affinity")
        {
            i++;
            std::string affinity_string(argv[i]);
            affinity = std::stoi(affinity_string);
            if (verbosity >= 2)
                PrintInfo((affinity) ? "Affinity enabled" : "Affinity disabled");
        }
        else if (arg == "-hdr")
        {
            i++;
            std::string hdr_string(argv[i]);
            hdr = bool(std::stoi(hdr_string));
            if (verbosity >= 2)
                PrintInfo((hdr) ? "HDR training data enabled" : "HDR training data disabled");
            if (!hdr)
            {
                PrintInfo("Enabling sRGB mode due to LDR");
                srgb = true;
            }
        }
        else if (arg == "-srgb")
        {
            i++;
            std::string srgb_string(argv[i]);
            srgb = bool(std::stoi(srgb_string));
            if (verbosity >= 2)
                PrintInfo((srgb) ? "sRGB mode enabled" : "sRGB mode disabled");
        }
        else if (arg == "-t")
        {
            i++;
            std::string num_threads_string(argv[i]);
            num_threads = std::stoi(num_threads_string);
            if (verbosity >= 2)
                PrintInfo("Number of threads set to %d", num_threads);
        }
        else if (arg == "-repeat")
        {
            i++;
            std::string repeat_string(argv[i]);
            num_runs = std::max(std::stoi(repeat_string), 1);
            if (verbosity >= 2)
                PrintInfo("Number of repeats set to %d", num_runs);
        }
        else if (arg == "-maxmem")
        {
            i++;
            std::string maxmem_string(argv[i]);
            maxmem = float(std::stoi(maxmem_string));
            if (verbosity >= 2)
                PrintInfo("Maximum denoiser memory set to %dMB", maxmem);
        }
        else if (arg == "-clean_aux")
        {
            i++;
            std::string clean_aux_string(argv[i]);
            clean_aux = bool(std::stoi(clean_aux_string));
            if (verbosity >= 2)
                PrintInfo((clean_aux) ? "cleanAux enabled" : "cleanAux disabled");
        }
        else if (arg == "-p")
        {
            i++;
            std::string prefilter_string(argv[i]);
            prefilter = bool(std::stoi(prefilter_string));
            if (verbosity >= 2)
                PrintInfo((prefilter) ? "prefiltering enabled" : "prefiltering disabled");
        }
        else if (arg == "-h" || arg == "--help")
        {
            printParams();
        }

    }
    
    if (srgb && hdr)
    {
        PrintInfo("Disabling sRGB, incompatble with HDR input");
        srgb = false;
    }


    // Check for a file extension
    int x = (int)out_path.find_last_of(".");
    x++;
    const char* ext_c = out_path.c_str() + x;
    std::string ext(ext_c);
    if (!ext.size())
    {
        PrintError("No output file extension");
        cleanup();
        exitfunc(EXIT_FAILURE);
    }
    
    //std::cout << "Denoiser denoiser\n";
    Denoiser denoiser;

    denoiser.init(
        affinity,
        hdr,
        srgb,
        num_runs,
        num_threads,
        maxmem,
        clean_aux,
        prefilter
    );
    
    //std::cout << "MultiChannelEXR exr\n";
    MultiChannelEXR exr;
    exr.setInput(in_path);
    exr.setOutput(out_path);

    //std::cout << "createBuffers\n";
    if (!exr.createBuffers(denoiser.getDevice()))
    {
        PrintError("No input image could be loaded");
        cleanup();
        exr.cleanup();
        exitfunc(EXIT_FAILURE);
    }


    if (!denoiser.denoise(exr))
    {
        exr.cleanup();
        cleanup();
        exitfunc(EXIT_FAILURE);
    }
    
    // If the image already exists delete it
    remove(out_path.c_str());

    if (exr.update())
    {
        PrintInfo("\nSaving to: %s", out_path.c_str());
        PrintInfo("Done!");
    }
    else
    {
        PrintError("\nCould not save file %s", out_path.c_str());
    }

    exr.cleanup();
    exitfunc(EXIT_SUCCESS);
}

