#!/usr/bin/bash

set -e

echo -e ==================== CONFIGURE ====================
EXTRACT_PATH=$1
BUILD_PATH=$2
INSTALL_PATH=${REZ_BUILD_INSTALL_PATH}
PACKAGE_NAME=${REZ_BUILD_PROJECT_NAME}
PACKAGE_VERSION=${REZ_BUILD_PROJECT_VERSION}
echo -e "EXTRACT_PATH: ${EXTRACT_PATH}"
echo -e "BUILD_PATH ${BUILD_PATH}"
echo -e "INSTALL_PATH: ${INSTALL_PATH}"
echo -e "PACKAGE_NAME: ${PACKAGE_NAME}"
echo -e "PACKAGE_VERSION: ${PACKAGE_VERSION}"
if [[ -z ${EXTRACT_PATH} || -z ${BUILD_PATH} || -z ${INSTALL_PATH} || -z ${PACKAGE_NAME} || -z ${PACKAGE_VERSION} ]]; then
    echo -e "One or more of the argument variables are empty. Aborting..."
    exit 1
fi

echo -e "START: ${PACKAGE_NAME}-${PACKAGE_VERSION}"
############### START ###############
#-DCMAKE_INSTALL_BINDIR=bin \
# Standalone build
mkdir -p ${BUILD_PATH}
cd ${BUILD_PATH}
cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} \
      -DCMAKE_C_FLAGS="-fPIC" \
      -DCMAKE_CXX_FLAGS="-fPIC ${CXXFLAGS}" \
      -DCMAKE_MODULE_PATH=${CMAKE_MODULE_PATH} \
      \
      -DOIDN_ROOT=${OIDN_ROOT} \
      -DOIIO_INCLUDE_PATH=${OIIO_INCLUDE_PATH} \
      -DOIIO_LIBRARY_PATH=${OIIO_LIBRARY_PATH} \
      -DIMATH_INCLUDE_PATH=${IMATH_INCLUDE_PATH} \
      -DIMATH_LIBRARY_PATH=${IMATH_LIBRARY_PATH} \
      \
      ${EXTRACT_PATH}




############### END ###############
echo -e "END: ${PACKAGE_NAME}-${PACKAGE_VERSION}"
