#!/usr/bin/bash

set -e

echo -e ==================== BUILD ====================
EXTRACT_PATH=$1
BUILD_PATH=$2
INSTALL_PATH=${REZ_BUILD_INSTALL_PATH}
PACKAGE_NAME=${REZ_BUILD_PROJECT_NAME}
PACKAGE_VERSION=${REZ_BUILD_PROJECT_VERSION}
echo -e "EXTRACT_PATH: ${EXTRACT_PATH}"
echo -e "BUILD_PATH ${BUILD_PATH}"
echo -e "INSTALL_PATH: ${INSTALL_PATH}"
echo -e "PACKAGE_NAME: ${PACKAGE_NAME}"
echo -e "PACKAGE_VERSION: ${PACKAGE_VERSION}"
if [[ -z ${EXTRACT_PATH} || -z ${BUILD_PATH} || -z ${INSTALL_PATH} || -z ${PACKAGE_NAME} || -z ${PACKAGE_VERSION} ]]; then
    echo -e "One or more of the argument variables are empty. Aborting..."
    exit 1
fi

echo -e "START: ${PACKAGE_NAME}-${PACKAGE_VERSION}"
############### START ###############


cd ${BUILD_PATH}
make -j${REZ_BUILD_THREAD_COUNT}


############### END ###############
echo -e "END: ${PACKAGE_NAME}-${PACKAGE_VERSION}"
