name = "denoiser"

version = "2.0.0"

authors = [
    "Matteo Manganelli"
]

description = \
    """
    Open Image Denoiser command line tool
    """

requires = [
    "cmake-3+",
    "gcc-9",
    "oiio",
    "imath",
]

hashed_variants = True

variants = [
    #["platform-linux", "oidn-1.4.3"],
    ["platform-linux", "oidn-2.0.1"],
]

#tools = []

build_system = "cmake"

with scope("config") as config:
    config.build_thread_count = "logical_cores"

uuid = "milkdenoiser-{version}".format(version=str(version))

def commands():

    env.MILKDENOISER_ROOT = "{root}/bin"
