- Init rez
source /milk/rez/rez/rez_init.sh

rez-env vscode gcc cmake oiio imath oidn-1.4.3
#rez-env vscode gcc cmake oiio imath oidn-2.0.1

#to build the denoiser with cmake
./configure.h
cd build
make
make install

#to build the denoiser with rez-build
#do not create an enviroment with rez-env but just call rez_init.sh
#cd in the project rez directory and type
rez-build --install
#the package will be installed in ~/package/package_name

#for using the denoiser type 
rez-env oidn-1.4.3 denoiser
or 
rez-env oidn-2.0.1 denoiser

#the denoiser executable path will be in the env var $MILKDENOISER_ROOT

#for example:
$MILKDENOISER_ROOT/Denoiser2 -i "my_image.exr" -o "my_image_Denoised.exr" -hdr 1 -repeat 2 -p 1